﻿using System.Text.Json.Serialization;

namespace LambdaASP.NETCoreMinimalAPI.QuyLH5.Models;

public class Customer
{
    [JsonPropertyName("pk")]
    public string Pk => Id.ToString();

    [JsonPropertyName("sk")] 
    public string Sk => Id.ToString();

    public Guid Id { get; set; } = Guid.NewGuid();
    
    public string Name { get; set; } = string.Empty;
}

public class CustomerForCreateDto
{
    public string Name { get; set; } = string.Empty;
}