using System.ComponentModel.DataAnnotations;
using LambdaASP.NETCoreMinimalAPI.QuyLH5.Models;
using LambdaASP.NETCoreMinimalAPI.QuyLH5.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LambdaASP.NETCoreMinimalAPI.QuyLH5.Controllers;

[ApiController]
[Route("[controller]")]
public class CustomerController : ControllerBase
{
    private readonly ILogger<CustomerController> _logger;
    private readonly ICustomerRepository _customerRepository;

    public CustomerController(ILogger<CustomerController> logger, ICustomerRepository customerRepository)
    {
        _logger = logger;
        _customerRepository = customerRepository;
    }


    [HttpPost("")]
    public async Task<IActionResult> AddCustomer([FromBody]CustomerForCreateDto customerForCreateDto)
    {
        var customer = new Customer
        {
            Name = customerForCreateDto.Name
        };
        
        return Ok(await _customerRepository.CreateAsync(customer));
    }


    [HttpGet("{id}")]
    public async Task<IActionResult> Get(string id)
    {
        _logger.LogInformation($"Get customer {id}");

        var customer = await _customerRepository.GetAsync(Guid.Parse(id));
        return Ok(customer);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(string id)
    {
        _logger.LogInformation($"Delete customer {id}");
        
        var result = await _customerRepository.DeleteAsync(Guid.Parse(id));
        return Ok(result);
    }
}