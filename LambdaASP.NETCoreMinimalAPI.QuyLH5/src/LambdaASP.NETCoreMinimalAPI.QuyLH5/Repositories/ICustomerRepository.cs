﻿using LambdaASP.NETCoreMinimalAPI.QuyLH5.Models;

namespace LambdaASP.NETCoreMinimalAPI.QuyLH5.Repositories;

public interface ICustomerRepository
{
    Task<Customer> CreateAsync(Customer customer);

    Task<Customer?> GetAsync(Guid id);

    Task<Customer> UpdateAsync(Customer customer);

    Task<bool> DeleteAsync(Guid id);
}