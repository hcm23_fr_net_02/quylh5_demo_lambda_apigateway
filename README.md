# quylh5_demo_lambda_apigateway



## Getting started

Install Amazon.Lambda.Tools package. Link: https://github.com/aws/aws-lambda-dotnet

After installing the package, you can create a new project using the template provided by the package.

## DynamoDB

You first need to create DynamoDb table.

Choose create table and fill in the table name.
![img_1.png](img_1.png)

![img_2.png](img_2.png)

To use DynamoDB in .NET, you need to install AWSSDK.DynamoDBv2 package and set up the dependency injection:

```csharp
// Add DynamoDB support, use RegionEndpoint.APSoutheast1 for Singapore region
builder.Services.AddSingleton<IAmazonDynamoDB>(_ => new AmazonDynamoDBClient(RegionEndpoint.APSoutheast1));

// Add CustomerRepository 
builder.Services.AddSingleton<ICustomerRepository>(provider =>
    new CustomerRepository(provider.GetRequiredService<IAmazonDynamoDB>(),
        config.GetValue<string>("Database:TableName")));
```

Note: You need to add the following configuration to aws-lambda-tools-defaults.json file:
![img.png](img.png)

## Deploy lambda function

To deploy lambda function, you need to run the following command:

```
dotnet lambda deploy-function
```

After choosing the function name, you have to grant the lambda function permission to access DynamoDB table.

1. Choose the function name, then go to Configuration tab, then choose Permissions. Click the roleName to go to IAM console.

![img_3.png](img_3.png)
2. Choose Attach policies, then search for DynamoDBFullAccess and attach it.

![img_4.png](img_4.png)

3. After attaching the policy, you will have the following policies attached to the role.

![img_5.png](img_5.png)

## API Gateway

To use API Gateway for the lambda function, you need to go to your specific lambda function, then choose Add trigger, then choose API Gateway.

![img_6.png](img_6.png)

Then go to API Gateway console, choose the API Gateway that you just created. In Routes, choose Create, then choose the route you want to create.
In this example, I will create a GET route using customer id.

![img_7.png](img_7.png)

Then choose Create. After getting back to Routes screen, choose Manage integration and intergrate with the lambda function that you want.

![img_8.png](img_8.png)

After this step, you can execute the lambda function by the endpoints: url/default/customer/{id}
